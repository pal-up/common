package com.palup.utilities;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Trie(dictionary)
 * <p>
 * Created by Kuldeep Yadav on 12/12/15.
 */
public class Trie {

    /**
     * Character to index map.
     */
    private Map<Character, Integer> characterIndexMap;

    /**
     * Index to character map.
     */
    private char[] indexCharacterMap;

    /**
     * The root node.
     */
    private Node root;

    /**
     * Stack containing last nodes for typed prefixes
     */
    private Stack<Node> stackOnTheFlyNodes;

    /**
     * Stack containing flags if prefix was present in trie
     */
    private Stack<Boolean> typedPrefixPresenceHistory;

    public Trie(char[] alphabet) {

        this.indexCharacterMap = alphabet;

        characterIndexMap = new HashMap<Character, Integer>();
        for (int i = 0; i < alphabet.length; i++) {
            this.characterIndexMap.put(alphabet[i], i);
        }
        Node.setAlphabetSize(alphabet.length);

        root = new Node();
        stackOnTheFlyNodes = new Stack<Node>();
        stackOnTheFlyNodes.push(root);
        typedPrefixPresenceHistory = new Stack<Boolean>();
    }

    public Trie(Map<Character, Integer> characterIndexMap) {

        this.characterIndexMap = characterIndexMap;
        this.indexCharacterMap = new char[characterIndexMap.size()];

        for (Map.Entry<Character, Integer> entry : characterIndexMap.entrySet()) {
            indexCharacterMap[entry.getValue()] = entry.getKey();
        }
        Node.setAlphabetSize(characterIndexMap.size());

        root = new Node();
    }

    /**
     * Insert a word.
     *
     * @param word word to be inserted.
     */
    public int insert(String word) {

        Node nodePtr = root;

        for (int i = 0; i < word.length(); i++) {

            if (nodePtr == null) {
                nodePtr = new Node();
            }
            int index = characterIndexMap.get(word.charAt(i));
            if (i == word.length() - 1) {
                return nodePtr.getElementArray()[index].incrementWordCount();
            } else {
                if (nodePtr.getElementArray()[index].getNextNode() == null) {
                    nodePtr.getElementArray()[index].setNextNode();
                }
                nodePtr = nodePtr.getElementArray()[index].getNextNode();
            }
        }
        throw new RuntimeException("word can not have 0 length");
    }

    /**
     * @param word word to be searched.
     * @return count of instances of searched words.
     */
    private int countInstances(String word) {
        Element lastElement = getLastElement(word);
        if (lastElement == null) {
            return 0;
        }
        return lastElement.getWordCount();
    }

    /**
     * @param word word to be searched
     * @return last element in the word.
     */
    private Element getLastElement(String word) {
        Node nodePtr = root;
        for (int i = 0; i < word.length(); i++) {

            if (nodePtr == null) {
                return null;
            }
            int index = characterIndexMap.get(word.charAt(i));
            if (i == word.length() - 1) {
                return nodePtr.getElementArray()[index];
            } else {
                nodePtr = nodePtr.getElementArray()[index].getNextNode();
            }
        }
        throw new RuntimeException("Should not reach here");
    }

    /**
     * @param word word to be searched.
     * @return true if the word exists in {@link Trie}.
     */
    public boolean isPresent(String word) {
        return countInstances(word) > 0;
    }

    /**
     * Delete a word from Tire.
     *
     * @param word word to be deleted.
     */
    public void delete(String word) {
        Element lastElement = getLastElement(word);
        if (lastElement == null) {
            throw new RuntimeException("The word does not exists in dictionary");
        }
        lastElement.setWordCount(0);
    }

    /**
     * @param nextChar next character typed
     * @return true if the partial typed word exists in trie, otherwise false
     */
    public boolean checkTyped(char nextChar) {

        Node nodeOnTop = stackOnTheFlyNodes.peek();
        if (nodeOnTop == null) {
            stackOnTheFlyNodes.add(null);
            typedPrefixPresenceHistory.push(false);
            return false;
        }
        int index = characterIndexMap.get(nextChar);
        stackOnTheFlyNodes.push(nodeOnTop.getElementArray()[index].getNextNode());
        boolean present = nodeOnTop.getElementArray()[index].getWordCount() > 0;
        typedPrefixPresenceHistory.push(present);
        return present;
    }

    /**
     * @return true if prefix left after one deletion is a word in trie
     */
    public boolean checkDeleted() {

        stackOnTheFlyNodes.pop();
        typedPrefixPresenceHistory.pop();
        if (typedPrefixPresenceHistory.empty()) {
            return false;
        } else {
            return typedPrefixPresenceHistory.peek();
        }
    }

    /**
     * A Node in a Tire
     *
     * @author kuldeep
     */
    private static class Node {

        /**
         * Size of alphabet.
         */
        private static int ALPHABET_SIZE;

        /**
         * All elements.
         */
        private Element[] elementArray;

        public Node() {

            this.elementArray = new Element[ALPHABET_SIZE];
            for (int i = 0; i < ALPHABET_SIZE; i++) {
                this.elementArray[i] = new Element();
            }
        }

        /**
         * @return the ALPHABET_SIZE
         */
        public static int getAlphabetSize() {
            return Node.ALPHABET_SIZE;
        }

        /**
         * @param alphabetSize the ALPHABET_SIZE to set
         */
        public static void setAlphabetSize(int alphabetSize) {
            Node.ALPHABET_SIZE = alphabetSize;
        }

        /**
         * @return the elementArray
         */
        public Element[] getElementArray() {
            return elementArray;
        }

    }


    /**
     * Elements to be wrapped in {@link Node}.
     *
     * @author kuldeep
     */
    private static class Element {

        /**
         * Count of words ending here.
         * For a dictionary it is either 0 if no word ends here,
         * 1 if there is such word in dictionary.
         */
        private int wordCount;

        /**
         * Next {@link Node} from this Element.
         */
        private Node nextNode;

        public Element() {
            super();
            this.wordCount = 0;
        }

        /**
         * @return increase count by 1 and return updated value of count.
         */
        public int incrementWordCount() {
            wordCount++;
            return wordCount;
        }

        /**
         * @return the wordCount
         */
        public int getWordCount() {
            return wordCount;
        }

        /**
         * Set word count.
         */
        public void setWordCount(int wordCount) {
            this.wordCount = wordCount;
        }

        /**
         * @return the nextNode
         */
        public Node getNextNode() {
            return nextNode;
        }

        /**
         * Set the next {@link Node}.
         */
        public Node setNextNode() {
            nextNode = new Node();
            return nextNode;
        }
    }

}
