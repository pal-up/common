package com.palup.entity;

/**
 * User creation details
 * <p>
 * Created by kuldeep on 15/12/15.
 */
public class UserCreationDetails {

    /**
     * Name of user
     */
    private String name;

    /**
     * Email of user
     */
    private String email;

    /**
     * URL of user photo
     */
    private String photoUrl;

    // For jackson support
    public UserCreationDetails() {
    }

    public UserCreationDetails(String name, String email, String photoUrl) {
        this.name = name;
        this.email = email;
        this.photoUrl = photoUrl;
    }

    /**
     * @return name of user
     */
    public String getName() {
        return name;
    }

    /**
     * @return email of user
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return url of user photo
     */
    public String getPhotoUrl() {
        return photoUrl;
    }
}
