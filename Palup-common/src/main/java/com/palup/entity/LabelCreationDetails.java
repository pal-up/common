package com.palup.entity;

/**
 * Details for label to be created
 * <p>
 * Created by Kuldeep Yadav on 08-Dec-15.
 */
public class LabelCreationDetails {

    /**
     * The Label name
     */
    private String name;

    /**
     * Id of parent label
     */
    private String parentLabelId;

    public LabelCreationDetails(String name, String parentLabelId) {
        this.name = name;
        this.parentLabelId = parentLabelId;
    }

    // For Jackson support
    public LabelCreationDetails() {
    }

    /**
     * @return name, label is bearing
     */
    public String getName() {
        return name;
    }

    /**
     * @return id of parent label
     */
    public String getParentLabelId() {
        return parentLabelId;
    }
}
