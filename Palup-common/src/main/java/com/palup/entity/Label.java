package com.palup.entity;

/**
 * Label to tag the event
 * <p>
 * Created by kuldeep on 10/12/15.
 */
public class Label {

    /**
     * Name of label
     */
    private String name;

    /**
     * Label id
     */
    private String id;

    public Label(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public Label() {
        // For jackson support
    }

    /**
     * @return name of label
     */
    public String getName() {
        return name;
    }

    /**
     * @param name of label
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return id of label
     */
    public String getId() {
        return id;
    }

    /**
     * @param id of label
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }
}
