package com.palup.entity;

/**
 * Interest of a user
 * <p>
 * Created by kuldeep on 29/12/15.
 */
public class Interest {

    /**
     * The label interest is concerned
     */
    private Label label;

    /**
     * Interest flag
     */
    private boolean interested;

    public Interest(Label label, boolean interested) {
        this.label = label;
        this.interested = interested;
    }

    public Interest() {
        // empty constructor for jackson
    }

    /**
     * @return the interest label
     */
    public Label getLabel() {
        return label;
    }

    /**
     * @return true if interested
     */
    public boolean isInterested() {
        return interested;
    }

    @Override
    public String toString() {
        return label.toString();
    }
}
